# Overview

This is a specialisation project in IMT4888, where I look at CMUSphinx and analysing voice command from direct voice input. The goal of the project is to evaluate the accuracy of the analysis and if the voice commands can reliably be used as the main control input in games.

To run this project you will need the Java Development Kit and Gradle. Gradle is needed to include and build the sphinx4 dependencies for the project.

See https://cmusphinx.github.io/wiki/tutorial/ for a more info about CMUSphinx.

## How to make it work

[See the wiki](https://bitbucket.org/Terris113/cmusphinx/wiki/Home) to see what games are currently supported and what kind of setup you need to make it work.

## Disclaimer

I am sorry for all terrible code.
