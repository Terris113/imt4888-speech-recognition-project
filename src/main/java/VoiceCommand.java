import com.google.api.gax.core.CredentialsProvider;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.api.gax.rpc.ClientStream;
import com.google.api.gax.rpc.ResponseObserver;
import com.google.api.gax.rpc.StreamController;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.speech.v1.*;

import com.google.protobuf.ByteString;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;
import edu.cmu.sphinx.result.WordResult;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.DataLine.Info;
import javax.sound.sampled.TargetDataLine;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


/**
 * Class handling speech recognition and generating computer events to do actions
 * based on the recognised speech.
 */
public class VoiceCommand {

    /*
    * CMU resources
    */
    private static final String CMU_ACOUSTIC_MODEL = "resource:/edu/cmu/sphinx/models/en-us/en-us";
    private static final String CMU_DICTIONARY = "resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict";
    private static final String CMU_LANGUAGE_MODEL = "resource:/edu/cmu/sphinx/models/en-us/en-us.lm.bin";

    /*
    * Own resources
    */
    private static final String GAME_SHORT_DICTIONARY = "file:./res/dictionary/game_short.dic";
    private static final String GAME_SHORT_LANGUAGE_MODEL = "file:./res/lang_model/game_short.lm";
    private static final String POKEMON_GEN1_DICTIONARY = "file:./res/dictionary/pokemon_gen1.dic";
    private static final String POKEMON_GEN1_LANGUAGE_MODEL = "file:./res/lang_model/pokemon_gen1.lm";

    /*
    * Robot for controlling keyboard and mouse events
    */
    private final Robot robot;

    /*
    * Configuration for voice recognition
    */
    private Configuration configuration;

    public VoiceCommand() throws AWTException {

        robot = new Robot();

        configuration = new Configuration();
        configuration.setAcousticModelPath(CMU_ACOUSTIC_MODEL);
        configuration.setDictionaryPath(POKEMON_GEN1_DICTIONARY);
        configuration.setLanguageModelPath(POKEMON_GEN1_LANGUAGE_MODEL);
        configuration.setUseGrammar(false);
    }


    /**
     * Using CMUSphinx to analyse sound input, and generates key events accordingly.
     * @throws IOException
     */
    public void startVoiceRecognition() throws IOException {

        LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(configuration);

        recognizer.startRecognition(true);

        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {
            for (WordResult word : result.getWords()) {
                System.out.println(word.getWord().toString());

                switch (word.getWord().toString()) {
                    case "LEFT":
                        robotInteraction(KeyEvent.VK_LEFT);
                        break;
                    case "RIGHT":
                        robotInteraction((KeyEvent.VK_RIGHT));
                        break;
                    case "UP":
                        robotInteraction(KeyEvent.VK_UP);
                        break;
                    case "DOWN":
                        robotInteraction(KeyEvent.VK_DOWN);
                        break;
                    case "CHOOSE":
                        robotInteraction(KeyEvent.VK_Z);
                        break;
                    case "BACK":
                        robotInteraction(KeyEvent.VK_X);
                        break;
                    case "L-TRIGGER":
                        robotInteraction(KeyEvent.VK_A);
                        break;
                    case "R-TRIGGER":
                        robotInteraction(KeyEvent.VK_S);
                        break;
                    case "START":
                        robotInteraction(KeyEvent.VK_ENTER);
                        break;
                    case "SELECT":
                        robotInteraction(KeyEvent.VK_BACK_SPACE);
                        break;

                }
            }
        }
    }

    /**
     * Runs through a file, analysing the sound data.
     * @throws IOException
     */
    public void startVoiceFileTranscription() throws IOException {

        StreamSpeechRecognizer recognizer = new StreamSpeechRecognizer(configuration);
        InputStream stream = new FileInputStream(new File("./res/sound/test.wav"));

        recognizer.startRecognition(stream);
        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {
            System.out.format("Hypotheis: %s\n", result.getHypothesis());
        }

        recognizer.stopRecognition();
    }

    /**
     * Reads a bytestream of sound, and passes it to the Speech API where it is transcribed.
     * @throws Exception
     */
    public void googleMicRecognizer() throws Exception {

        /*
         * Credentials for Google, giving access to their API
         */
        CredentialsProvider credentialsProvider =
                FixedCredentialsProvider.create(ServiceAccountCredentials.fromStream(
                        new FileInputStream("./res/cred/credentials.json")
                ));
        SpeechSettings settings = SpeechSettings.newBuilder().setCredentialsProvider(credentialsProvider).build();

        ResponseObserver<StreamingRecognizeResponse> responseObserver = null;
        try (SpeechClient client = SpeechClient.create(settings)) {

            responseObserver =
                    new ResponseObserver<StreamingRecognizeResponse>() {
                        ArrayList<StreamingRecognizeResponse> responses = new ArrayList<>();

                        public void onStart(StreamController controller) {}

                        public void onResponse(StreamingRecognizeResponse response) {
                            responses.add(response);
                            StreamingRecognitionResult result = response.getResultsList().get(0);
                            SpeechRecognitionAlternative alternative = result.getAlternativesList().get(0);
                            String transcript = alternative.getTranscript();
                            System.out.printf("Input : %s\n", transcript);

                            // Hacky way of stopping the program
                            if(transcript.contains("stop program")) {
                                System.exit(1);
                            }

                            // Inserts the texts
                            insertText(transcript);
                        }

                        public void onComplete() { }

                        public void onError(Throwable t) {
                            System.out.printf("This is a throwable: %s", t);

                        }
                    };

            ClientStream<StreamingRecognizeRequest> clientStream =
                    client.streamingRecognizeCallable().splitCall(responseObserver);

            // Creating context for the API to easier detect words and sentence context
            SpeechContext.Builder speechBuilder = SpeechContext.newBuilder();
            speechBuilder.addPhrases("enemy");
            speechBuilder.addPhrases("climb");

            RecognitionConfig recognitionConfig =
                    RecognitionConfig.newBuilder()
                            .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16)
                            .setLanguageCode("en-US")
                            .setSampleRateHertz(16000)
                            .addSpeechContexts(speechBuilder)
                            .build();
            StreamingRecognitionConfig streamingRecognitionConfig =
                    StreamingRecognitionConfig.newBuilder().setConfig(recognitionConfig).build();

            // Setup request
            StreamingRecognizeRequest request =
                    StreamingRecognizeRequest.newBuilder()
                            .setStreamingConfig(streamingRecognitionConfig)
                            .build(); // The first request in a streaming call has to be a config

            clientStream.send(request);

            // SampleRate:16000Hz, SampleSizeInBits: 16, Number of channels: 1, Signed: true,
            // bigEndian: false
            AudioFormat audioFormat = new AudioFormat(16000, 16, 1, true, false);
            DataLine.Info targetInfo =
                    new Info(
                            TargetDataLine.class,
                            audioFormat); // Set the system information to read from the microphone audio stream

            if (!AudioSystem.isLineSupported(targetInfo)) {
                System.out.println("Microphone not supported");
                System.exit(0);
            }
            // Target data line captures the audio stream the microphone produces.
            TargetDataLine targetDataLine = (TargetDataLine) AudioSystem.getLine(targetInfo);
            targetDataLine.open(audioFormat);
            targetDataLine.start();
            System.out.println("Start speaking");

            // The Audio Input Stream for the program to read sound from
            AudioInputStream audio = new AudioInputStream(targetDataLine);

            // Need to keep track of the length of voice recognition, so that we don't
            // get a server-side throwable
            long startTime = System.currentTimeMillis();

            // Continously transcribing the data
            while (true) {
                long estimatedTime = System.currentTimeMillis() - startTime;

                // Need to restart the session after 60 seconds to avoid the throwable
                if (estimatedTime > 60000) {
                    System.out.println("Restarting");
                    targetDataLine.stop();
                    targetDataLine.close();
                    break;
                }
                byte[] data = new byte[6400];
                audio.read(data);
                sendGoogleRequest(clientStream, data);
            }
        } catch (Exception e) {
            System.out.printf("This is an exception: %s", e);
        }
        System.out.println("Session ended");
    }

    /**
     * Method for sending requests to Google Cloud API
     * @param clientStream is the open stream for sending requests
     * @param data is the voice data
     */
    private void sendGoogleRequest(ClientStream<StreamingRecognizeRequest> clientStream, byte[] data) {

        StreamingRecognizeRequest request =
                StreamingRecognizeRequest.newBuilder()
                        .setAudioContent(ByteString.copyFrom(data))
                        .build();

        clientStream.send(request);
    }

    /**
     * Method for doing keyboard events
     * @param eventNumber is the corresponding keycode
     */
    private void robotInteraction(int eventNumber) {

        int duration = 1000;
        long start = System.currentTimeMillis();
        // Simulating "holding button down", courtesy: https://stackoverflow.com/a/6642282
        while (System.currentTimeMillis() - start < duration) {
            robot.keyPress(eventNumber);
            robot.delay(100);
        }

        robot.keyPress(eventNumber);
        robot.delay(100);
        robot.keyRelease(eventNumber);
    }

    /**
     * Method for inserting text into a text game (or any other text program), this is done by adding
     * the text to the clipboard and simply pasting it into the needed text field
     * @param text is the text to be inserted
     */
    private void insertText(String text) {
        StringSelection stringSelection = new StringSelection(text);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, stringSelection);

        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.delay(100);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);

        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(100);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.mousePress(MouseEvent.BUTTON1);
        robot.mouseRelease(MouseEvent.BUTTON1);
    }

    public static void main(String[] args) throws Exception  {

       VoiceCommand voiceCommand = new VoiceCommand();

//       voiceCommand.startVoiceRecognition();

        // Looping so that we avoid the time limit of Google Cloud
        // Hacky solutions to bypass stupid restrictions
        while(true) {
            voiceCommand.googleMicRecognizer();
        }
    }
}
